#include<iostream>
#include<iomanip>
using namespace std;

int main() 
{
    int a[3][3], b[3][3], c[3][3],i,j;

    for(i=0;i<3;i++)
    {
        for(j=0;j<3;j++)
        {
            cout<<endl<<"Enter the value of a: ";
            cin>>a[i][j];

            cout<<endl<<"Enter the value of b: ";
            cin>>b[i][j];

            c[i][j]=a[i][j]+b[i][j];

        }

    }
    
   for(i=0;i<3;i++)
    {
        for(j=0;j<3;j++)
        {
            cout<<setw(4)<<a[i][j];
        }
        cout<<setw(2)<<" ";

        for(j=0;j<3;j++)
        {
            cout<<setw(4)<<b[i][j];
        }
        cout<<setw(2)<<" ";

        for(j=0;j<3;j++)
        {
            cout<<setw(4)<<c[i][j];
        }
        cout<<endl;
    }

    return 0;
}