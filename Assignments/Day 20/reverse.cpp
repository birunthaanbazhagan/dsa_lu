#include<iostream>
using namespace std;

void digit(int, int*, int*);

int main() {
    int num, rev, sum;

    cout<<endl<<"Enter the no.: ";
    cin>>num;

    digit(num, &rev, &sum);

    cout<<endl<<"Sum of digit of "<<num<<"is "<<sum;
     cout<<endl<<"rev number of "<<num<<"is "<<rev;

     return 0;

}

void digit(int n, int *r, int *s)
{
    int rem;
    *r=0;
    *s=0;

    while(n>0)
    {
        rem=n%10;
        *s+=rem;
        *r=(*r*10)+rem;
        n=n/10;
    }
}