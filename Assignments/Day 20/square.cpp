#include<iostream>
using namespace std;

void square(int,int*,int*);

int main() {
    int l,a,p;

    cout<<endl<<"Enter the length: ";
    cin>>l;

    square(l,&a,&p);

     cout<<endl<<"Area of Square: "<<a;
     cout<<endl<<"Perimeter of Square: "<<p;

    return 0;
}

void square(int l, int *a, int *p)
{
    *a=l*l;
    *p=4*l;
}