#include<iostream>
using namespace std;

void swap(int* , int *);

int main() {
    int a,b;

    cout<<endl<<"Enter a value: ";
    cin>>a;
     cout<<endl<<"Enter b value: ";
    cin>>b;

    swap(&a, &b);

    cout<<endl<<"Afer swapping: ";

    cout<<endl<<" A: "<<a;
     cout<<endl<<" B: "<<b;

     return 0;

}

void swap(int *p, int *q)
{
    *p=*p+*q;
    *q=*p-*q;
    *p=*p-*q;

}