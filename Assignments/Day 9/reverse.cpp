#include<iostream>
using namespace std;
int main() 
{
    int num, reversenum=0, remainder;

    cout<<endl<<"Enter 5 digit number : ";
    cin>>num;

    while(num!=0)
    {
        remainder = num % 10;
        reversenum = reversenum *10 + remainder;
        num/= 10;
    }
    cout<<endl<<"Reversed Number = "<<reversenum;
    
    return 0;
}