#include <iostream>

using namespace std;

int main()
{
    int A[3][3], Min, Max;

    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            cout<<endl<<"Enter the values: ";
            cin>>A[i][j];
        }
    }
    Max = A[0][0];
    Min = A[0][0];
    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            cout<<A[i][j]<<" ";

            if (A[i][j] > Max)
            {
                Max = A[i][j];
            }
            if (A[i][j] < Min)
            {
                Min = A[i][j];
            }
        }
        cout<<endl;
    }
    cout<<"Max: "<<Max<<endl;
    cout<<"Min: "<<Min<<endl;
    return 0;
}