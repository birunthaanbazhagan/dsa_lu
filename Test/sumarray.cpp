#include <iostream>
using namespace std;

int main()
{
    int a[5], n, sum[5], i, m;

    for (i = 0; i < 5; i++)
    {
        cout << "Enter number: ";
        cin >> a[i];

        n = a[i];
        sum[i] = 0;

        while (n > 0)
        {
            m = n % 10;
            n = n / 10;
            sum[i] = sum[i] + m;
        }
    }
    for (i = 0; i < 5; i++)
    {
        cout << endl
             << a[i] << "  " << sum[i];
    }
    return 0;
}