#include <iostream> 
using namespace std; 
int power(int a,int k) 
{ 
    int x=1; 
    for(int i=0;i<a;i++) 
    { 
        x*=k; 
    } 
    return x; 
} 
int main() 
{ 
    int n; 
    int dec=0; 
    cout<<"Enter the Number of Disks : "; 
    cin>>n; 
    dec=power(n,2)-1; 
    cout<<"The Minimum Number of Steps required is: "<<dec; 
    return 0; 
}