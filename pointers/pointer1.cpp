#include<iostream>
using namespace std;

int main() {
    int a=25;
    int a, *p=&a;
    float b=3.8;
    float *q=&b;

    // cout<<endl<<"value a = ";
    // cin>>a;
    
    cout<<endl<<"Address of A: "<<&a;
    cout<<endl<<"Value at A: "<<a;

    cout<<endl<<"Address of *P: "<<&p;
    cout<<endl<<"Value at P: "<<p;

    cout<<endl<<"Pointer p(value at address store in P): "<<*p;

    return 0;

}