#include<iostream>
using namespace std;

void calc(int,int,int*,int*);

int main() {
    int l,b,a,p;

    cout<<endl<<"Enter the length: ";
    cin>>l;

    cout<<endl<<"Enter the breadth: ";
    cin>>b;

    calc(l,b,&a,&p);

     cout<<endl<<"Area of Rectangle: "<<a;
     cout<<endl<<"Perimeter of Rectangle: "<<p;

    return 0;
}

void calc(int l, int b, int *a, int *p)
{
    *a=l*b;
    *p=2*(l+b);
}