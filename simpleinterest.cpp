// C++ Program to Calculate Simple Interest
#include <iostream>
using namespace std;

int main() {
    
    int p, n, r, SI;
    
    cout << "Enter the principal amount: ";
    cin >> p;
    cout << "Enter the time period(in years): ";
    cin >> n;
    cout << "Enter the rate of interest: ";
    cin >> r;
    
    SI = (p*n*r) / 100;
   
    cout << "Simple Interest: " << SI << endl;
    return 0;
}