#include<iostream>
using namespace std;

// prototype means introducing the functions

void drawline(int);
void msg();

int main() 
{
    drawline(3); //calling of function
    
    msg();

    drawline(2);

    cout<<endl<<"Bye From Main";

    return 0;

}

void msg()
{
    drawline(1);
    cout<<endl<<"Hi , UDF Intro";
    drawline(1);
}

void drawline(int n)
{
    int i,j;

    for(i=1;i<=n;i++)
    {
        cout<<endl;
        for(j=1;j<=70;j++)
        {
            cout<<"*";
        }
    }
}